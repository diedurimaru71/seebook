<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Student;

class MobaController extends Controller
{
    
    public function signin(Request $info){

        try {
            
            $student = Student::where('nis', $info->nis)->where('password', $info->password)->first();
            return response()->json([

                'message' => 'Login Berhasil',
                'serve' => $student

            ], 200);

        } catch (Exception $errmsg) {

            return response()->json([

                'message' => 'Yahh Gagal',
                'serve' => []

            ], 500);
            
        }

    }

}
